//
//  ClassifiedResponse.swift
//  ClassifiedDemo
//
//  Created by Takvir Hossain Tusher on 21/12/20.
//  Copyright © 2020 Demo. All rights reserved.
//

import Foundation

// Classified Response Model
struct ClassifiedResponse : Codable {
    let results : [ClassifiedResults]?
    let pagination : Pagination?

    enum CodingKeys: String, CodingKey {

        case results = "results"
        case pagination = "pagination"
    }

    init() {
        results = [ClassifiedResults]()
        pagination = Pagination()
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        results = try values.decodeIfPresent([ClassifiedResults].self, forKey: .results)
        pagination = try values.decodeIfPresent(Pagination.self, forKey: .pagination)
    }

}

// Results Model
struct ClassifiedResults : Codable {
    let createdAt : String?
    let price : String?
    let name : String?
    let uid : String?
    let imageIds : [String]?
    let imageUrls : [String]?
    let imageUrlsThumbnails : [String]?

    enum CodingKeys: String, CodingKey {

        case createdAt = "created_at"
        case price = "price"
        case name = "name"
        case uid = "uid"
        case imageIds = "image_ids"
        case imageUrls = "image_urls"
        case imageUrlsThumbnails = "image_urls_thumbnails"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt)
        price = try values.decodeIfPresent(String.self, forKey: .price)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        uid = try values.decodeIfPresent(String.self, forKey: .uid)
        imageIds = try values.decodeIfPresent([String].self, forKey: .imageIds)
        imageUrls = try values.decodeIfPresent([String].self, forKey: .imageUrls)
        imageUrlsThumbnails = try values.decodeIfPresent([String].self, forKey: .imageUrlsThumbnails)
    }

}

// Pagination Model
struct Pagination : Codable {
    let key : String?

    init() {
        key = ""
    }
    
    enum CodingKeys: String, CodingKey {

        case key = "key"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        key = try values.decodeIfPresent(String.self, forKey: .key)
    }

}
