//
//  ClassifiedListInteractor.swift
//  ClassifiedDemo
//
//  Created by Takvir Hossain Tusher on 21/12/20.
//  Copyright © 2020 Demo. All rights reserved.
//

import Foundation

protocol ClassifiedInteractor {
    func fetchClassifiedItems(completion: @escaping ClassifiedClosure) -> Void
}

class ClassifiedListInteractor {
    var classifiedService: ClassifiedAPI
    
    init(service: ClassifiedAPI) {
        self.classifiedService = service
    }
}

extension ClassifiedListInteractor: ClassifiedInteractor {
    func fetchClassifiedItems(completion: @escaping ClassifiedClosure) {
        self.classifiedService.fetchClassifiedItems(completion: completion)
    }
}
