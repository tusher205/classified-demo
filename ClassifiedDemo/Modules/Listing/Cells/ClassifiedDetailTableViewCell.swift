//
//  ClassifiedDetailTableViewCell.swift
//  ClassifiedDemo
//
//  Created by Takvir Hossain Tusher on 21/12/20.
//  Copyright © 2020 Demo. All rights reserved.
//

import UIKit

class ClassifiedDetailTableViewCell: UITableViewCell {
    
    // MARK: Properties
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: Class Methods
    class func loadNib() -> UINib {
        return UINib(nibName: String(describing: ClassifiedDetailTableViewCell.classForCoder()),
                     bundle: .main)
    }

    class func reuseIdentifier() -> String {
        return String(describing: ClassifiedDetailTableViewCell.classForCoder())
    }
}
