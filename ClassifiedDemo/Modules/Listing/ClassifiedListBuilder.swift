//
//  ClassifiedListBuilder.swift
//  ClassifiedDemo
//
//  Created by Takvir Hossain Tusher on 21/12/20.
//  Copyright © 2020 Demo. All rights reserved.
//

import UIKit

class ClassifiedListBuilder {
    
    static func build() -> UIViewController {
        let view = ClassifiedListViewController()
        let classifiedInteractor = ClassifiedListInteractor(service: ClassifiedService.shared)
        let imageInteractor = ImageInteractor(service: ClassifiedService.shared)
        let router = ClassifiedListRouter(view: view)
        let presenter = ClassifiedListPresenter(view: view,
                                                router: router,
                                                useCase: (
                                                    fetchItems: classifiedInteractor.fetchClassifiedItems,
                                                    fetchThumbnail: imageInteractor.fetchThumbnail,
                                                    fetchImage: imageInteractor.fetchImage
        ))
        
        view.presenter = presenter
        
        return view
    }
}
