//
//  ClassifiedListRouter.swift
//  ClassifiedDemo
//
//  Created by Takvir Hossain Tusher on 21/12/20.
//  Copyright © 2020 Demo. All rights reserved.
//

import UIKit

protocol ClassifiedRouter {
    
}

class ClassifiedListRouter {
    var viewController: UIViewController
    
    init(view: UIViewController) {
        self.viewController = view
    }
}

extension ClassifiedListRouter: ClassifiedRouter {
    
}
