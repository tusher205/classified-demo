//
//  ClassifiedListConstants.swift
//  ClassifiedDemo
//
//  Created by Takvir Hossain Tusher on 20/12/20.
//  Copyright © 2020 Demo. All rights reserved.
//

import Foundation

enum ClassifiedListSectionIdentifier: Int {
    case section1 = 0
    case sectionCount // Keep at the last item
}

enum ClassifiedDetailIdentifier: Int {
    case itemPrice = 0
    case itemUid
    case itemPostedDate
    case itemCount// Keep at the last item
}

enum ViewUpdateState: Int {
    case featchItems = 0
    case fetchThumbnail
    case fetchImage
    case fetchError
}
