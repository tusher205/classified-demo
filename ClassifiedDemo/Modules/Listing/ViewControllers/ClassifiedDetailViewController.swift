//
//  ClassifiedDetailViewController.swift
//  ClassifiedDemo
//
//  Created by Takvir Hossain Tusher on 21/12/20.
//  Copyright © 2020 Demo. All rights reserved.
//

import UIKit

class ClassifiedDetailViewController: UIViewController {
    
    // MARK: Properties    
    @IBOutlet weak var imageScrollView: UIScrollView!
    @IBOutlet weak var imagePageControl: UIPageControl!
    @IBOutlet weak var itemPriceLabel: UILabel!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemDetailTableView: UITableView!
    
    var presenter: ClassifiedPresenter?
    var classifiedItem: ClassifiedResults?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = CLLocalizeText.TEXT_ITEM_DETAILS
        setUpView()
    }
    
    // MARK: Methods
    private func setUpView() {
        
        // Setup Image scroll view
        setUpImageScrollView()
        
        // Setup product detail table view
        setUpProductDetailTableView()
        
        // Set name, price label
        itemNameLabel.text = classifiedItem?.name?.capitalized
        if let price = classifiedItem?.price {
            itemPriceLabel.text = price.uppercased()
        }
    }
    
    private func setUpImageScrollView() {
        imageScrollView.isPagingEnabled = true
        imageScrollView.showsVerticalScrollIndicator = false
        imageScrollView.showsHorizontalScrollIndicator = false
        imageScrollView.frame = CGRect(x: 0,
                                       y:0,
                                       width: UIScreen.main.bounds.width,
                                       height: UIScreen.main.bounds.width)
        
        // Add image view to scroll view
        setUpImagesToScrollView(classifiedItem?.imageUrls ?? [String]())
        
        // Set page control
        setUpPageControl(classifiedItem?.imageUrls?.count ?? 0)
    }
    
    private func setUpImagesToScrollView(_ images: [String]) {
        
        // Set images into scroll view
        for i in 0..<images.count {
            let imageView = UIImageView()
            
            // Load images asynchronously
            self.presenter?.onFetchImage(name: images[i]) { image in
                imageView.image = image
            }
            
            imageView.image = UIImage(named: "defaultPhoto") // Default
            imageView.contentMode = .scaleAspectFit
            
            let xPosition = UIScreen.main.bounds.width * CGFloat(i)
            imageView.frame = CGRect(x: xPosition,
                                     y: 0,
                                     width: imageScrollView.frame.width,
                                     height: imageScrollView.frame.height)
            
            imageScrollView.contentSize.width = imageScrollView.frame.width * CGFloat(i + 1)
            imageScrollView.addSubview(imageView)
            imageScrollView.delegate = self
        }
    }
    
    private func setUpPageControl(_ count: Int) {
        imagePageControl.numberOfPages = count
        imagePageControl.currentPage = 0
        view.bringSubviewToFront(imagePageControl)
    }
    
    private func setUpProductDetailTableView() {
        itemDetailTableView.dataSource = self
        itemDetailTableView.delegate = self
        itemDetailTableView.tableFooterView = UIView()
        
        itemDetailTableView.register(ClassifiedDetailTableViewCell.loadNib(),
                                forCellReuseIdentifier: ClassifiedDetailTableViewCell.reuseIdentifier())
    }
}

// MARK: UIScrollViewDelegate
extension ClassifiedDetailViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x/view.frame.width)
        imagePageControl.currentPage = Int(pageIndex)
    }
}

// MARK: UITableViewDataSource
extension ClassifiedDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ClassifiedDetailIdentifier.itemCount.rawValue
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = ClassifiedDetailTableViewCell.reuseIdentifier()
        
        guard let cell = tableView
            .dequeueReusableCell(withIdentifier: cellIdentifier,
                                 for: indexPath) as? ClassifiedDetailTableViewCell else {
            
            print("The dequeued cell is not an instance of ClassifiedDetailViewController")
            return UITableViewCell()
        }

        let detailId = ClassifiedDetailIdentifier(rawValue: indexPath.row) ?? .itemCount
        
        switch detailId {
        case .itemPrice:
            cell.titleLabel.text = CLLocalizeText.TEXT_PRICE
            cell.detailLabel.text = classifiedItem?.price
            
        case .itemPostedDate:
            cell.titleLabel.text = CLLocalizeText.TEXT_POSTED_AT
            cell.detailLabel.text = DateUtils
                .getFormatedDateFromString(classifiedItem?.createdAt ?? "")
            
        case .itemUid:
            cell.titleLabel.text = CLLocalizeText.TEXT_PRODUCT_ID
            cell.detailLabel.text = classifiedItem?.uid
            
        case .itemCount:
            fallthrough
            
        @unknown default:
            return cell
        }
        
        // Cell Style
        cell.backgroundColor = UIColor.systemGray5
        cell.selectionStyle = .none
        
        return cell
    }
    
}

// MARK: UITableViewDelegate
extension ClassifiedDetailViewController: UITableViewDelegate {
    
}
