//
//  ClassifiedListViewController.swift
//  ClassifiedDemo
//
//  Created by Takvir Hossain Tusher on 21/12/20.
//  Copyright © 2020 Demo. All rights reserved.
//

import UIKit

protocol ClassifiedView: class {
    func showClassifiedItems()
    func showError()
}

class ClassifiedListViewController: UIViewController {
    
    // MARK: Properties
    @IBOutlet weak var itemsTableView: UITableView!
    
    var presenter: ClassifiedPresenter?
    var loadingTimer: Timer? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = CLLocalizeText.TEXT_CLASSIFIED_ITEMS

        setUpTableView()
        presenter?.viewDidLoad()
        
        self.startLoadingPopup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.loadingTimer?.invalidate()
        self.loadingTimer = nil
    }
    
    // MARK: Methods
    private func setUpTableView() {
        itemsTableView.dataSource = self
        itemsTableView.delegate = self
        itemsTableView.tableFooterView = UIView()
        
        itemsTableView.register(ClassifiedListTableViewCell.loadNib(),
                                forCellReuseIdentifier: ClassifiedListTableViewCell.reuseIdentifier())
    }
    
    private func pushToDetailViewController(for index: Int) {
        let vc = ClassifiedDetailViewController()
        let item = presenter?.getClassifiedItem(at: index)
        vc.classifiedItem = item
        vc.presenter = self.presenter
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK:- UITableViewDataSource
extension ClassifiedListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return ClassifiedListSectionIdentifier.sectionCount.rawValue
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let sectionId = ClassifiedListSectionIdentifier(rawValue: section) ?? .sectionCount
        
        switch sectionId {
        case .section1:
            let count = presenter?.getClassifiedItemsCount() ?? 0
            return count
            
        case .sectionCount:
            fallthrough
            
        @unknown default:
            return 0
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = ClassifiedListTableViewCell.reuseIdentifier()
        
        guard let cell = tableView
            .dequeueReusableCell(withIdentifier: cellIdentifier,
                                 for: indexPath) as? ClassifiedListTableViewCell else {
            
            print("The dequeued cell is not an instance of ClassifiedDetailTableViewCell")
            return UITableViewCell()
        }
        
        // Fetches the appropriate item for the data source layout.
        let item = presenter?.getClassifiedItem(at: indexPath.row)
        
        cell.itemNameLabel.text = item?.name?.capitalized ?? ""
        cell.itemPriceLabel.text = item?.price ?? "0"
        
        if let imageName = item?.imageUrlsThumbnails?.first {
            self.presenter?.onFetchThumbnail(name: imageName) { image in
                cell.itemThumbnailImageView.image = image
            }
            
            cell.itemThumbnailImageView.image = UIImage(named: "defaultPhoto")
        }
        
        // Style Cell
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = .none
        
        return cell
    }
}

// MARK:- UITableViewDelegate
extension ClassifiedListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        self.pushToDetailViewController(for: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let itemCount = presenter?.getClassifiedItemsCount() ?? 0
        return "\(itemCount) Items"
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}

extension ClassifiedListViewController: ClassifiedView {
    func showClassifiedItems() {
        self.stopLoadingPopup()
        itemsTableView.reloadData()
    }
    
    func showError() {
        // Stop loading
        self.stopLoadingPopup()
        
        let alert = UIAlertController(title: CLLocalizeText.TEXT_ALERT,
                                      message: CLLocalizeText.TEXT_PROBLEM_FETCHING_CLASSIFIED_ITEMS,
                                      preferredStyle: UIAlertController.Style.alert)
        
        let alertOkAction = UIAlertAction(title: CLLocalizeText.TEXT_OK,
                                          style: .default, handler: {(_: UIAlertAction) in
            
            alert.dismiss(animated: true, completion: nil)
        })
        
        alert.addAction(alertOkAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension ClassifiedListViewController {
    
    func startLoadingPopup() {
        self.startLoadingTimer()
    }
    
    func stopLoadingPopup() {
        self.stopLoadingTimer()
    }
    
    func startLoadingTimer() -> Void {
        self.loadingTimer?.invalidate()
        self.loadingTimer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(actionOnTimerExpire), userInfo: nil, repeats: false)
    }
    
    @objc func actionOnTimerExpire() -> Void {
        self.stopLoadingPopup()
    }
    
    func stopLoadingTimer() -> Void {
        self.loadingTimer?.invalidate()
        self.loadingTimer = nil
    }
}

