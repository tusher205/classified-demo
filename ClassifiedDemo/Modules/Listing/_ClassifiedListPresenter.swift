//
//  ClassifiedListPresenter.swift
//  ClassifiedDemo
//
//  Created by Takvir Hossain Tusher on 21/12/20.
//  Copyright © 2020 Demo. All rights reserved.
//

import Foundation

protocol ClassifiedPresenter {
    func viewDidLoad() -> Void
    func getClassifiedItemsCount() -> Int?
    func getClassifiedItem(at index: Int) -> ClassifiedResults?
    
    func onFetchThumbnail(name: String, completion: @escaping ImageClosure)
    func onFetchImage(name: String, completion: @escaping ImageClosure)
}

class ClassifiedListPresenter {
    
    // MARK: Properties
    weak var view: ClassifiedView?
    var router: ClassifiedRouter
    
    typealias UseCase = (
        fetchItems: (_ completion: @escaping ClassifiedClosure) -> Void,
    fetchThumbnail: (_ name: String, _ completion: @escaping ImageClosure) -> Void,
        fetchImage: (_ name: String, _ completion: @escaping ImageClosure) -> Void
    )
    
    var interactors: UseCase?
    
    var classifiedItems: [ClassifiedResults]?
    
    init(view: ClassifiedView,
         router: ClassifiedRouter,
         useCase: ClassifiedListPresenter.UseCase) {
        
        self.view = view
        self.router = router
        self.interactors = useCase
    }
}

extension ClassifiedListPresenter: ClassifiedPresenter {
    
    func viewDidLoad() {
        if self.classifiedItems?.count ?? 0 > 0 {
            // Items already featched
            self.updateView(for: .featchItems)
        } else {
            self.fetchItems()
        }
    }
    
    func getClassifiedItemsCount() -> Int? {
        return self.classifiedItems?.count
    }
    
    func getClassifiedItem(at index: Int) -> ClassifiedResults? {
        if index >= getClassifiedItemsCount() ?? 0 {
            print("Invalid index")
            return nil
        }
        
        return self.classifiedItems?[index]
    }
    
    func onFetchThumbnail(name: String, completion: @escaping ImageClosure) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            self?.interactors?.fetchThumbnail(name) { (image) in
                DispatchQueue.main.async {
                    completion(image)
                }
            }
        }
    }
    
    func onFetchImage(name: String, completion: @escaping ImageClosure) {
        DispatchQueue.global(qos: .background).async { [weak self] in
            self?.interactors?.fetchImage(name) { (image) in
                DispatchQueue.main.async {
                    completion(image)
                }
            }
        }
    }
    
}

extension ClassifiedListPresenter {
    func fetchItems() {
        DispatchQueue.global(qos: .background).async { [weak self] in
            self?.interactors?.fetchItems { (response) in
                if let results = response.results, results.count > 0 {
                    print("Received Classified Items: \(results.count)")
                    self?.classifiedItems = results
                    self?.updateView(for: .featchItems)
                } else {
                    self?.updateView(for: .fetchError)
                }
            }
        }
    }
    
    func updateView(for state: ViewUpdateState) {
        DispatchQueue.main.async { [weak self] in
            switch state {
            case .featchItems:
                self?.view?.showClassifiedItems()
                
            case .fetchImage, .fetchThumbnail:
                break
                
            case .fetchError:
                self?.view?.showError()
            }
        }
    }
    
}
