//
//  ClassifiedListViewControllerTest.swift
//  ClassifiedDemoTests
//
//  Created by Takvir Hossain Tusher on 22/12/20.
//  Copyright © 2020 Demo. All rights reserved.
//

import XCTest
@testable import ClassifiedDemo

class ClassifiedListViewControllerTest: XCTestCase {
    let presenter = ClassifiedPresenterMock()
    
    func makeView() -> ClassifiedListViewController {
        let view = ClassifiedListViewController()
        view.presenter = presenter
        view.loadViewIfNeeded()
        return view
    }
    
    func testViewDidLoadPresenterCall() {
        let view = makeView()
        view.viewDidLoad()
        
        XCTAssertTrue(presenter.viewDidLoadCalled)
    }

}

class ClassifiedPresenterMock: ClassifiedPresenter {
    
    private (set) var viewDidLoadCalled = false
    
    func viewDidLoad() {
        viewDidLoadCalled = true
    }
    
    private (set) var getClassifiedItemsCountCalled = false
    
    func getClassifiedItemsCount() -> Int? {
        getClassifiedItemsCountCalled = true
        return 0
    }
    
    private (set) var getClassifiedItemCalled = false
    
    func getClassifiedItem(at index: Int) -> ClassifiedResults? {
        getClassifiedItemCalled = true
        return nil
    }
    
    private (set) var onFetchThumbnailCalled = false
    
    func onFetchThumbnail(name: String, completion: @escaping ImageClosure) {
        onFetchThumbnailCalled = true
    }
    
    private (set) var onFetchImageCalled = false
    
    func onFetchImage(name: String, completion: @escaping ImageClosure) {
        onFetchImageCalled = true
    }
    
}

